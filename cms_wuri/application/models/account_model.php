<?php
class Account_model extends CI_Model 
{
	function simpan_register() 
	{
		$simpan_data = array (
			'username' => $this->input->post('username'),
			'name' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'umur' => $this->input->post('umur'),
			'password' => md5($this->input->post('password')),
			'id' => null,	
			'active' => 'Y',	
			'usergroup' => '2'	
		);
		
		$simpan = $this->db->insert('users', $simpan_data);
		return $simpan;
	}
	
	function cek_row() 
	{
		$username = $this->input->post('username');
		$pass = md5($this->input->post('password'));
		$query = $this->db->select("id");
		$query = $this->db->where("username", $username);
		$query = $this->db->where("password", $pass);
		$query = $this->db->get("users");
		
		return $query->num_rows();
	}
	
	function data_user() 
	{
		$username = $this->input->post('username');
		$pass = md5($this->input->post('password'));
		$query = $this->db->select("*");
		$query = $this->db->where("username", $username);
		$query = $this->db->where("password", $pass);
		$query = $this->db->get("users");

		return $query->result();
	}
}
