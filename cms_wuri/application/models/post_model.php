<?php
class Post_model extends CI_Model 
{
	function simpan_post($data) 
	{
		/*
		 * $simpan_data = array (
			'id' => $this->gen_id(),
			'judul' => $this->input->post('judul'),
			'isi' => $this->input->post('isi'),
			'gambar' => $filename,
			'author' => $this->session->userdata('id_user')
		);
		*/
		
		$simpan = $this->db->insert('post', $data);
		return $simpan;
	}

	function update_post($id, $filename) 
	{
		$simpan_data = array (
			'judul' => $this->input->post('judul'),
			'isi' => $this->input->post('isi'),
			'gambar' => $filename
		);
		
		$this->db->where('id', $id);
		$this->db->update('post', $simpan_data);
	}
	
	function get_posts($limit = '', $offset = '', $aksi = '', $param = '') 
	{
		$query = $this->db->select("*");
		if ($this->session->userdata('id_user') != 'tamu' && $this->session->userdata('id_user') != '' ) {
			$query = $this->db->where('author', $this->session->userdata('id_user'));
		//echo $this->session->userdata('id_user');exit;
		}

		if ($aksi == 'search') {
			$query = $this->db->like('judul', $param);
		} else {
			$query = $this->db->limit($limit, $offset);			
		}
		
		
		$this->db->order_by("judul", "ASC"); 
		$query = $this->db->get('post');
		return $query->result();
	}

	function get_single_post($id) 
	{
		$query = $this->db->select("*");
		$query = $this->db->where('id', $id);		
		$query = $this->db->get('post');
		return $query->row_array();
	}

	function get_edit_post($id) 
	{
		$query = $this->db->select("*");
		$query = $this->db->where('id', $id);		
		$query = $this->db->get('post');
		return $query->row_array();
	}
	
	function get_val($field, $id) 
	{
		$query = $this->db->select($field);
		$query = $this->db->where('id', $id);		
		$query = $this->db->get('post');
		
		return $query->row_array()[$field];
	}
	
	function get_row() 
	{
		$query = $this->db->select("id");
		if ($this->session->userdata('id_user') != 'tamu') {
			$query = $this->db->where('author', $this->session->userdata('id_user'));
		}
		
		$query = $this->db->get("post");
		
		return $query->num_rows();
	}
	
	function gen_id($table) 
	{
		$date = date('d').date('m').date('Y');
		$jam = date('H').date('i').date('s');

		$query = $this->db->select('id');
		$query = $this->db->like('id', $date.$jam);		
		$query = $this->db->order_by('id','desc');
		$query = $this->db->get($table);

		if ($query->num_rows() > 0) {
			$id_max = $query->row_array()['id'];
		} else {
			$id_max = false;
		}

		$gen_id = $date.$jam.$this->session->userdata('id_user').rand(0,1000);
		
		if ($id_max == $gen_id) {
			return $this->gen_id();
		} else {
			return $gen_id;
		}
	}
	
	function delete_post($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('post');	
	}
}
