<?php
class User_model extends CI_Model{
	function simpan_insert_user() {
		$simpan_data = array (
			'nama' => $this->input->post('nama'),
		);
		$simpan = $this->db->insert('users', $simpan_data);
		return $simpan;
	}
	function simpan_edit_user($id, $nama) {
		$data = array(
					'id' => $id,
					'nama' => $nama
				);
		$this->db->where('id', $id);
		$this->db->update('users', $data);		
	}
	function get_user_all($aksi = '', $param = '') {
		$query = $this->db->select("*");
		
		if ($aksi == 'search') {
			$this->db->like('nama', "$param"); 
				
		} else {
			$query = $this->db->get('users');
		}
		$this->db->order_by("nama", "ASC"); 
		$query = $this->db->get('users');
		return $query->result();
	}
	function delete($id) {
		$this->db->where('id', $id);
		$this->db->delete('users');
	}
	function get_user($id) {
		$nama = NULL;
		$query = $this->db->select("*");
		$this->db->where('id', $this->db->escape($id));
		$query = $this->db->get('users');
		$arr = $query->result();
		foreach ($arr as $v) {
			$nama = $v->nama;
		}
		return $nama;
	}
}
