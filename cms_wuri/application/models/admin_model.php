<?php
class Admin_model extends CI_Model 
{	
	function get_tamu() 
	{
		$query = $this->db->select("*");
		
		if ($this->session->userdata('id_user') == 'tamu') {
			$query = $this->db->where('id !=', $this->session->userdata('tamu_id'));
		}

		//if ($aksi == 'search') {
		//	$query = $this->db->like('judul', "$param");
		//}
		//$query = $this->db->limit($limit, $offset);
		
		$this->db->order_by("tanggal", "DESC");
		$query = $this->db->get('buku_tamu');
		return $query->result();
	}
	
	function delete_tamu($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete('buku_tamu');
	}

}
