<html>
<head>
	<title><?php echo $title; ?></title>
	<meta charset="UTF-8">
	<!-- Styles css -->
	<link href="<?php echo base_url(); ?>lib/css/style.css" rel="stylesheet">
	<!-- scripts js -->
	<script src="js/javascript.js"></script>
</head>
<body>
	<header id='header'>
		<ul id='nav'>
			<li id='nav-1'><?php echo anchor( ( $this->session->userdata('logged') ? 'admin_controller' : 'post_controller' ) , 'HOME'); ?></li>
			<?php if ($this->session->userdata('logged')) : ?>
			<li id='nav-2'><?php echo anchor('post_controller/view_post', 'POST'); ?></li>
			<?php endif; ?>
			<li id='nav-3'><?php echo anchor('account_controller/logout', ( $this->session->userdata('logged') ? 'LOGOUT' : 'KELUAR' )); ?></li>				
		</ul>
	</header>
	<br />
		<div class="container">
		
			<?php echo $content; ?>
		
		</div>
	<footer id='footer'>
		<p>Copyright &copy; Abdi <?php echo date('Y'); ?></p>
	</footer>
</body>
</html>
