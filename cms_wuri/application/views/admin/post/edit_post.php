<?php echo ( isset($pesan) ? $pesan : null ); ?>
<?php echo form_open_multipart('post_controller/edit_post/'.$post['id'].'/save');?>
	<?php echo form_error('judul'); ?>
	<label>Judul : </label><br />
	<input type='text' placeholder=judul value="<?php echo $post['judul']; ?>" name='judul'/><br />
	<label>Gambar : </label><br />
	<input type='file' size="20" placeholder=gambar name='userfile'/><br />
	<?php if ($post['gambar']) : ?>
		<input type='hidden' name='cur_gambar' value='<?php echo $post['gambar']; ?>' />
	<?php endif; ?>
	<?php echo form_error('isi'); ?>
	<br />
	<label>Isi : </label><br />
	<textarea rows='10' name='isi'>
		<?php echo $post['isi']; ?>
	</textarea><br />
	<input type='submit' value='Save'>
</form>

<?php if ($post['gambar']) : ?>
	<?php echo form_open('post_controller/edit_post/'.$post['id'].'/remove_image'); ?>
	<label>Gambar : </label>
	<input type='text' disabled value='<?php echo $post['gambar']; ?>'>
	<input type='hidden' name='cur_gambar' value='<?php echo $post['gambar']; ?>' />
	<input type='submit' value='remove' />
	<?php echo form_close(); ?>
<?php endif; ?>

<?php echo anchor('post_controller/view_post', 'Daftar list'); ?>
