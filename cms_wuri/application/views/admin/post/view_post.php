<html>
<head>
	<style>
		#testim table {
			text-align: center;
			border-collapse : collapse;
			width : 100%;
			border-color : blueviolet;
		}
		#testim table th {
			color: tomato;
			background : lightblue;
		}
	</style>
	<title>Posts list</title>
	<meta charset="UTF-8">
	<!-- Styles css -->
	<link href="css/style.css" rel="stylesheet">
	<!-- scripts js -->
	<script src="js/javascript.js"></script>
</head>
<body>
	<?php if ($this->session->userdata('logged')) : ?>
		<h1><?php echo $author; ?>'s posts</h1>
	<?php  endif; ?>
		<div class="container">
			<?php echo form_open('post_controller/view_post/search');?>
				<input type='search' value='<?php set_value('search'); ?>' autofocus name='search' />
				<input type='submit' value='search' />
			</form>
			<table border='1'>
				<tr>
					<th>NO</th>
					<th>JUDUL</th>
					<th>ISI</th>
					<th>OPSI</th>
				</tr>
				<?php
				$nomor = $offset + 1;
				foreach ($posts as $post) {
					echo "<tr>
						<td>$nomor</td>
						<td>".$post->judul."</td>
						<td>".$post->isi."</td>
						<td>
							".anchor('post_controller/single_post/'.$post->id, 'view')."
							";
					if ($this->session->userdata('logged')) {
						echo anchor('post_controller/edit_post/'.$post->id, 'edit');
						?>
						<td><a href='<?php echo base_url().'index.php/post_controller/delete_post/'.$post->id; ?>' onclick='return confirm("Anda yakin ?")'>Hapus</a></td>
						<?php
					}					
					echo "		
						</td>
					</tr>";
				$nomor ++;
				}
				?>
			</table>
			<?php echo $link_page; ?>
			<br />
			<?php if ($this->session->userdata('logged')) : ?>
			<?php echo anchor('post_controller/add_new', 'New Post'); ?>
			<?php  endif; ?>
			
			<!-- testimonial -->
			<p>TESTIMONIAL</p>
			<?php
			echo '
			<div id="testim">
			<table border=1>
				<tr>
					<th>Nama</th>
					<th>Pesan & Kesan</th>
					<th>Tanggal</th>
				</tr>
			';
			foreach ($tamu_list as $tamu) {
				echo "
				<tr>
					<td>".$tamu->nama."</td>
					<td>".$tamu->pesan."</td>
					<td>".$tamu->tanggal."</td>				
				</tr>
				
				";
			}
			echo '</table>
			</div>';
			echo "<br>";
			?>
			
			
			<?php echo anchor('account_controller/logout', ( $this->session->userdata('logged') ? 'logout' : 'keluar' )); ?>
			<?php if ($this->session->userdata('logged')) echo anchor('admin_controller', 'Dashboard'); ?>
		</div>
	<!-- script additional -->
	<script src="js/additional.js"></script>
</body>
</html>
