<html>
<head>
	<title><?php echo $post['judul']; ?></title>
	<meta charset="UTF-8">
	<!-- Styles css -->
	<link href="css/style.css" rel="stylesheet">
	<!-- scripts js -->
	<script src="js/javascript.js"></script>
</head>
<body>
	<?php echo $this->session->flashdata('pesan_flash') ; ?>
	<h1><?php echo $post['judul']; ?></h1>
		<div class="container">
			<span style='color:lightblue'><?php echo 'On '.$post['tgl_buat']; ?></span> ||
			<span style='color:lightblue'><?php echo 'Athor '.$this->session->userdata('username'); ?></span>
			<br />
			<?php echo $post['isi']; ?><br />
			<?php if ($post['gambar']) : ?>
				<img src="<?php echo base_url()."uploads/".$post['gambar']; ?>" width='200' height='200' />
			<?php endif; ?>
			<?php echo '<br />'.anchor('post_controller/view_post/', 'daftar post'); ?>
		</div>
	<!-- script additional -->
	<script src="js/additional.js"></script>
</body>
</html>
