<html>
<head>
	<title><?php echo $judul; ?></title>
	<meta charset="UTF-8">
</head>
<body>
	<div class="container">
		SELAMAT DATANG
		<span style='color:green;font-weight:bold;'>
			<?php if ($this->session->userdata('logged')) : ?>
				<?php echo $this->session->userdata('nama') ?>
			<?php else :?>
				TAMU
			<?php endif; ?>
		</span>
		<span style='margin-left:40px;'>
			<?php if ($this->session->userdata('logged')) : ?>
				<a href="<?php echo base_url().'index.php/account_controller/logout'; ?>">LOGOUT</a>
			<?php else : ?>
				<a href="<?php echo base_url().'index.php/account_controller/'; ?>">LOGIN</a>
			<?php endif; ?>
		</span>
		<br />
		<?php if ($this->session->userdata('logged')) : ?>
			<button><a style='text-decoration:none;' href="<?php echo base_url().'index.php/user_controller/user/insert'; ?>">Tambah</a></button>	
		<?php endif; ?>
		
		<form method='POST' action="<?php echo base_url().'index.php/user_controller/user/search'; ?>">
			<input type='search' name='search' />
			<input type='submit' value='Search' />
		</form>
		<table border='1'>
			<tr>
				<th>NO</th>
				<th>ID</th>
				<th>NAMA</th>
				<th>CREATED AT</th>
				<?php if ($this->session->userdata('logged')) : ?>
					<th>OPSI</th>
				<?php endif ?>
			</tr>
			<?php $no = 1; foreach ($daftar_user as $v) : ?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $v->id; ?></td>
					<td><?php echo $v->nama; ?></td>
					<td><?php echo $v->created_at; ?></td>
					<?php if ($this->session->userdata('logged')) : ?>
						<td>
							<a onclick="return confirm('apakah anda yakin?');" href="<?php echo base_url().'index.php/user_controller/delete/'.$v->id; ?>">delete</a>
							<a href="<?php echo base_url().'index.php/user_controller/user/edit/'.$v->id; ?>">edit</a>
						</td>		
					<?php endif; ?>
				</tr>
			<?php $no++; endforeach; ?>
		</table>
		<br />
		<?php if ($aksi) : ?>
			<?php
			if ($aksi == 'insert') {
				$url = base_url().'index.php/user_controller/simpan_user/insert';
			}else if ($aksi == 'edit') {
				$url = base_url().'index.php/user_controller/simpan_user/edit';				
			}
			?>
			<form action="<?php echo $url; ?>" method="POST">
				<table border=1>
					<tr>
						<td>NAMA : </td>
						<td><input type="text" name="nama" value="<?php echo (empty($edit_nama)?'':$edit_nama); ?>" placeholder="nama" /></td>
						<input type='hidden' name='id' value="<?php echo (empty($id)?'':$id) ?>"/>
					</tr>
					<tr>
						<td ><input type='submit' value='simpan' /></td>
						<td><button><a style="text-decoration:none;" href="<?php echo base_url().'index.php/user_controller/user'; ?>">cancel</a></button></td>
					</tr>
				</table>
			</form>
		<?php endif; ?>
	</div>
</body>
</html>
