<?php
class Admin_controller extends CI_Controller 
{
	
	function __construct () 
	{
		parent ::__construct();
		if (!$this->session->userdata('logged')) {
			redirect('account_controller');
		}
		$this->load->model('admin_model');
	}
	
	function index() 
	{
		$this->view_home();
	}
	
	function view_home() 
	{
		$data['list_tamu'] = $this->admin_model->get_tamu();
		$data['judul'] = 'Home';
		$data['title'] = 'Admin Home';
		$data['content'] = $this->load->view('admin/home', $data, true);
		$this->load->view('main', $data);
	}
	
	function delete_tamu($id) 
	{
		if ($this->session->userdata('usergroup') != 1) {
			redirect('admin_controller');
		}
		$this->admin_model->delete_tamu($id);
		redirect('admin_controller');
	}

}
