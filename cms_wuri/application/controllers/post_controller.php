<?php
class Post_controller extends CI_Controller 
{
	var $limit = 3;
	//var $logged = $this->session->userdata('logged');
	
	function __construct () 
	{
		parent ::__construct();
		$this->load->library('form_validation');
		$this->load->model('post_model');
		$data['logged'] = $this->session->userdata('logged');	
	}
	
	function index() 
	{
		//
		$this->view_post();			
	}

	function add_new($aksi = '') 
	{
		if (!$this->session->userdata('logged')) {
			redirect('account_controller');
		}		
		if ($aksi == 'proses_add_post') {
			$conf_validasi = array(
				array(
					 'field'   => 'judul', 
					 'label'   => 'Judul', 
					 'rules'   => 'required'
				  )
			);
			
			$this->form_validation->set_rules($conf_validasi);			
		
			$msg_error = array(
				'required' => 'Data harus diisi'
			);

			$this->form_validation->set_message($msg_error);
			
			if (!$this->form_validation->run()) {
				$this->form_validation->set_error_delimiters('<div style="color:red">', '</div>');
			} else {
				$conf_upload['upload_path'] = './uploads/';
				$conf_upload['allowed_types'] = 'gif|jpg|png';
				$conf_upload['max_size']	= '100';
				$conf_upload['max_width']  = '1024';
				$conf_upload['max_height']  = '768';				
								
				
				$this->load->library('upload', $conf_upload);
				if (! $this->upload->do_upload()) {
					$data['error_uploads'] = $this->upload->display_errors();
					$filename = '';
				} else {
					//$data['data_upload'] = $this->upload->data();
					$filename = $this->upload->data()['file_name'];
				}
				
				//data post
				$id_post = $this->post_model->gen_id('post');
				$data = array (
							'id' => $id_post,
							'judul' => $this->input->post('judul'),
							'isi' => $this->input->post('isi'),
							'gambar' => $filename,
							'author' => $this->session->userdata('id_user')
						);
				
				$this->post_model->simpan_post($data);
				$this->session->set_flashdata('pesan_flash', 'Tambah post sukses !');
				redirect('post_controller/single_post/'.$id_post);
				//$data['pesan'] = "Post baru berhasil ditambahkan";
			}
					
		}	//end new_post
		
		$data['title'] = 'Add new Post';
		$data['judul'] = 'Add new Post';
		$data['content'] = $this->load->view('admin/post/new_post', $data, true);			
		$this->load->view('main', $data);
	}

	function view_post($aksi = '')	//semua posts
	{
		//$data['logged'] = $this->logged;
		$this->load->library('pagination');

		$config['base_url'] = 'http://localhost/cms_wuri/index.php/post_controller/view_post/';
		$config['total_rows'] = $this->post_model->get_row();
		$config['per_page'] = $this->limit;
		$offset = $this->uri->segment(3);
		
		$this->pagination->initialize($config);

		$data['link_page'] = $this->pagination->create_links();
		
		$data['author'] = $this->session->userdata('username');
		
		if ($aksi == 'search') {
			$data['posts'] = $this->post_model->get_posts($this->limit, $offset + 1, $aksi, $this->input->post('search'));
		} else {
			$data['posts'] = $this->post_model->get_posts($this->limit, $offset);
		}
		
		$this->load->model('admin_model');
		$data['tamu_list'] = $this->admin_model->get_tamu();
		
		$data['title'] = 'View Post';
		$data['offset'] = $offset;
		$data['judul'] = 'View Post';
		$data['content'] = $this->load->view('admin/post/view_post', $data, true);
		$this->load->view('main', $data);
	}
	
	function single_post($id)
	{
		$data['author'] = $this->session->userdata('username');
		$data['post'] = $this->post_model->get_single_post($id);
		$data['title'] = 'View Post';
		$data['judul'] = 'View Post';
		$data['content'] = $this->load->view('admin/post/single_post', $data, true);		
		$this->load->view('main', $data);
	}
	
	function edit_post($id, $aksi = '') 
	{
		if (!$this->session->userdata('logged')) {
			redirect('account_controller');
		}
		if ($aksi == 'save') {
			$conf_validasi = array(
				array(
					 'field'   => 'judul', 
					 'label'   => 'Judul', 
					 'rules'   => 'required'
				  )
			);
			
			$this->form_validation->set_rules($conf_validasi);			
		
			$msg_error = array(
				'required' => 'Data harus diisi'
			);

			$this->form_validation->set_message($msg_error);
			
			if (!$this->form_validation->run()) {
				$this->form_validation->set_error_delimiters('<div style="color:red">', '</div>');
			} else {
				$conf_upload['upload_path'] = './uploads/';
				$conf_upload['allowed_types'] = 'gif|jpg|png';
				$conf_upload['max_size']	= '100';
				$conf_upload['max_width']  = '1024';
				$conf_upload['max_height']  = '768';				
				
				
				$this->load->library('upload', $conf_upload);
				if (! $this->upload->do_upload()) {
					$data['error_uploads'] = $this->upload->display_errors();
					$data['data_upload']['file_name'] = $this->post_model->get_val('gambar', $id);
				} else {
					$data['data_upload'] = $this->upload->data();
					if ($this->input->post('cur_gambar') != $this->upload->data()['file_name'] && !empty($this->input->post('cur_gambar'))) {
						unlink('./uploads/'.$this->input->post('cur_gambar'));
					}
				}
				
				$this->post_model->update_post($id, $data['data_upload']['file_name']);
				$data['pesan'] = 'Post berhasil disimpan';
			}
		} else if ($aksi == 'remove_image') {
			unlink('./uploads/'.$this->input->post('cur_gambar'));
			$this->db->where('id', $id);
			$this->db->update('post', array('gambar' => ''));	
		}
		
		$data['post'] = $this->post_model->get_edit_post($id);
		$data['title'] = 'Edit Post';
		$data['judul'] = 'Edit Post';
		$data['content'] = $this->load->view('admin/post/edit_post', $data, true);	
		$this->load->view('main', $data);			
	}

	function delete_post($id)
	{
		if (!$this->session->userdata('logged')) {
			redirect('account_controller');
		}		
		$this->post_model->delete_post($id);
		redirect('post_controller');	
	}

	function test() 
	{
		$this->load->view('template');
	}

}
