<?php
class User_controller extends CI_Controller {
	
	function __construct () {
		parent ::__construct();
	}
	function index() {
		redirect('account_controller/index');		
	}
	function test() {
		$data['judul'] = 'test';
		$this->load->view('template', $data);					
	}
	function user($aksi = '', $id = '') {
		$this->load->model('user_model');
		if ($aksi == 'search') {
			$nama = $this->input->post('search');
			$data['daftar_user'] = $this->user_model->get_user_all($aksi ,$nama);		
		}else {
			$data['daftar_user'] = $this->user_model->get_user_all();
		}
		$data['aksi'] = '';
		if ($aksi == 'insert') {
			$data['aksi'] = 'insert';
		}else if ($aksi == 'edit') {
			$data['aksi'] = 'edit';
			$data['id'] = $id;
			$data['edit_nama'] = $this->user_model->get_user($id);
			if ($data['edit_nama'] == NULL) {
				show_404();
			}
		}
		$data['judul'] = 'View Data User';
		$this->load->view('view_user', $data);			
	}
	function simpan_user($jenis) {
		$this->load->model('user_model');
		if ($jenis == 'insert') {
			$this->user_model->simpan_insert_user();
		}else if ($jenis == 'edit') {
			$id = $this->input->post('id');
			$nama = $this->input->post('nama');
			$data['edit'] = $this->user_model->simpan_edit_user($id, $nama);
		}
		redirect('user_controller/user/');
	}
	function delete($id) {
		$this->load->model('user_model');
		$this->user_model->delete($id);
		redirect('user_controller/user/');
	}
}
