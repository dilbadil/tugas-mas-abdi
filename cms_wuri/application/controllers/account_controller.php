<?php
class Account_controller extends CI_Controller {
	
	function __construct () {
		parent ::__construct();
		$this->load->library('form_validation');
		$this->load->model('post_model');
	}
	
	function index() 
	{
		$data['judul'] = 'login';
		$this->load->view('login/login', $data);			
	}
	
	function login($aksi = '') 
	{
		$data['judul'] = 'login';
		if ($aksi = 'sukses_register') {
			//$data['notifikasi'] = 'Sukses Register';		
		}

		$this->load->view('login/login', $data);			
	}
	
	function proses_login() 
	{
		$this->load->model('account_model');
		
		$config = array(
					   array(
							 'field'   => 'username', 
							 'label'   => 'Username', 
							 'rules'   => 'required'
						  ),
					   array(
							 'field'   => 'password', 
							 'label'   => 'Password', 
							 'rules'   => 'required'
						  )
					);
					
		$this->form_validation->set_rules($config);		
		
		$num = $this->account_model->cek_row();
		$data_user = $this->account_model->data_user();
		if ($num > 0) {
			foreach ($data_user as $k => $v) {
				$id = $v->id;
				$ug = $v->usergroup;
			}
			
			$array = array(
						'username' => $this->input->post('username'),
						'logged' => true,
						'id_user' => $id,
						'usergroup' => $ug
					);
			$this->session->set_userdata($array);
			redirect('admin_controller');
			
		} else {
			$data['judul'] = 'login';
			$data['notifikasi'] = 'Gagal bro !';
			$this->load->view('login/login', $data);
		}
	}
	
	function register() 
	{
		$data['judul'] = 'Registrasi';
		$this->load->view('register/register', $data);
	}
	
	function proses_register() 
	{
		$data['judul'] = 'Registrasi';
		$config = array(
					   array(
							 'field'   => 'nama', 
							 'label'   => 'Nama', 
							 'rules'   => 'required|callback_alpha_dash_space'
						  ),
					   array(
							 'field'   => 'email', 
							 'label'   => 'Email', 
							 'rules'   => 'valid_email|is_unique[users.email]'
						  ),
					   array(
							 'field'   => 'username', 
							 'label'   => 'Username', 
							 'rules'   => 'required|is_unique[users.username]'
						  ),
					   array(
							 'field'   => 'umur', 
							 'label'   => 'Umur', 
							 'rules'   => 'required|numeric|greater_than[15]|less_than[100]'
						  ),
					   array(
							 'field'   => 'password', 
							 'label'   => 'Password', 
							 'rules'   => 'required'
						  ),
					   array(
							 'field'   => 'password_confirm', 
							 'label'   => 'Konfirmasi Password', 
							 'rules'   => 'required|matches[password]'
						  )
					);
		$this->form_validation->set_rules($config);
		
		$msg_error = array(
			'alpha_dash_space' => 'Nama tidak valid',
			'required' => 'Data harus diisi'
		);
		
		$this->form_validation->set_message($msg_error);

		if (!$this->form_validation->run()) {
			$this->form_validation->set_error_delimiters('<div style="color:red">', '</div>');
		} else {
			$this->load->model('account_model');
			$this->account_model->simpan_register();
			redirect('account_controller/login/sukses_register');
		}
		
		$this->load->view('register/register',$data);
	}
	
	function alpha_dash_space($str) 
	{
		if ( ! preg_match("/^([-a-z_ ])+$/i", $str)) {
			return false;
		}else {
			return true;
		}
	}
	
	function logout() 
	{
		if ($this->session->userdata('id_user') == 'tamu') {
			$data['title'] = 'Tamu';
			$this->load->view('tamu/tamu_out', $data);
		} else {
			$this->session->sess_destroy();
			redirect('account_controller');
		}
	}

	function login_tamu() 
	{
		$id = $this->post_model->gen_id('buku_tamu');
		$this->load->model('post_model');
		$tamu = $this->input->post('tamu');
		$simpan_data = array (
			'nama' => $this->input->post('tamu'),
			'id' => $id
		);

		$array = array(
					'username' => $tamu,
					'id_user' => 'tamu',
					'tamu_id' => $id
				);
		$this->session->set_userdata($array);
		
		$simpan = $this->db->insert('buku_tamu', $simpan_data);
		redirect('post_controller');		
	}
	
	function add_pesan_tamu() 
	{
		$tamu_id = $this->session->userdata('tamu_id');
		$simpan_data = array (
			'pesan' => $this->input->post('pesan')
		);
		
		$this->db->where('id', $tamu_id);
		$this->db->update('buku_tamu', $simpan_data);
		$this->session->sess_destroy();
		redirect('account_controller');
	}
	
	function upload_bro()
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array(
						'error' => $this->upload->display_errors()
					);

			$this->load->view('register/register', $error);
		}
		else
		{
			$data = array(
						'do_upload' => true,
						'upload_data' => $this->upload->data()
					);

			$this->load->view('register/register', $data);
		}
	}	
}
