<?php
session_start();
require_once('setting.php');
	echo "SELAMAT DATANG : ";
	echo "<span style='color:green;font-weight:bold;'>".(empty($_SESSION['nama'])?'Tamu':$_SESSION['nama'])."</span>";
	?>
	<br/>
	<?php if (isset($_SESSION['logged'])) : if ($_SESSION['logged'] == true) : ?>
		<button><a style='text-decoration:none;' href="?aksi=tambah">Tambah</a></button>
	<?php  endif; endif; ?>
	<table border='1'>
		<tr>
			<th>NO</th>
			<th>ID</th>
			<th>NAMA</th>
			<th>CREATED AT</th>
			<?php if (isset($_SESSION['logged'])) : if ($_SESSION['logged'] == true) : ?>
				<th>OPSI</th>
			<?php endif; endif; ?>
		</tr>
		<?php
		$q_view = mysqli_query($con,"SELECT * FROM users");
		$nomor = 1;
		while ($row = mysqli_fetch_array($q_view)) :
			?>
			<tr>
				<td><?php echo $nomor; ?></td>
				<td><?php echo $row['id']; ?></td>
				<td><?php echo $row['nama']; ?></td>
				<td><?php echo $row['created_at']; ?></td>
				<?php if (isset($_SESSION['logged'])) : if ($_SESSION['logged'] == true) : ?>
					<td>
						<a onclick="return confirm('apakah anda yakin?');" href="?aksi=delete&id=<?php echo $row['id']; ?>">delete</a>
						<a href="?aksi=edit&id=<?php echo $row['id']; ?>">edit</a>
					</td>
				<?php endif; endif; ?>
			</tr>
			<?php
			$nomor++;
		endwhile;
		?>
	</table>
	<?php
?>
<br/>
<?php
if (!empty($_GET)) {
	if (isset($_GET['pesan'])) {
		if ($_GET['pesan'] == 'hapus_sukses') {
			echo "Hapus user sukses !";
		}else if ($_GET['pesan'] == 'hapus_gagal') {
			echo "Hapus user gagal !";
		}else if ($_GET['pesan'] == 'edit_sukses') {
			echo "Edit user sukses !";
		}else if ($_GET['pesan'] == 'edit_gagal') {
			echo "Edit user gagal !";
		}else if ($_GET['pesan'] == 'cancel') {
			echo "Canceled !";
		}
	}
	if (isset($_GET['aksi'])) {
		if ($_GET['aksi'] == 'logout') {
			unset($_SESSION['nama']);
			unset($_SESSION['logged']);
			echo "<script>window.location = 'masuk.php';</script>";
		}else if ($_GET['aksi'] == 'delete') {
			$id = mysqli_real_escape_string($con,$_GET['id']);
			if (mysqli_query($con,"DELETE FROM users WHERE id='$id'")) {
				echo "<script>window.location = 'session-view.php?pesan=hapus_sukses';</script>";			
			}else {
				echo "<script>window.location = 'session-view.php?pesan=hapus_gagal';</script>";			
			}
		}else if ($_GET['aksi'] == 'edit') {
			$id = mysqli_real_escape_string($con,$_GET['id']);
			$view_nama = mysqli_fetch_array(mysqli_query($con,"SELECT nama FROM users WHERE id='$id'"));
			?>
			Masukan nama baru untuk user <span style='color:green;font-weight:bold;'><?php echo $view_nama['nama']; ?></span>
			<form method='GET'>
				<input type='text' name='nama' autofocus='autofocus' value="<?php echo $view_nama['nama']; ?>"/>
				<input type='hidden' name='aksi' value='tombol_edit'>
				<input type='hidden' name='id' value="<?php echo $id; ?>">
				<input type='submit' value='ok'/>
			</form>
			<button name='cancel'><a style='text-decoration:none;' href="?aksi=cancel">cancel</a></button>
			<?php
		}else if ($_GET['aksi'] == 'tombol_edit') {
			$nama = mysqli_real_escape_string($con,$_GET['nama']);
			$id = mysqli_real_escape_string($con,$_GET['id']);
			if (mysqli_query($con,"UPDATE users SET nama='$nama' WHERE id='$id'")) {
				echo "<script>window.location = 'session-view.php?pesan=edit_sukses';</script>";			
			}else {
				echo "<script>window.location = 'session-view.php?pesan=edit_gagal';</script>";			
			}
		}else if ($_GET['aksi'] == 'tombol_tambah') {
			$nama = mysqli_real_escape_string($con,$_GET['nama']);
			if (mysqli_query($con,"INSERT INTO users (nama) VALUES ('$nama')")) {
				echo "<script>window.location = 'session-view.php?pesan=tambah_sukses';</script>";			
			}else {
				echo "<script>window.location = 'session-view.php?pesan=tambah_gagal';</script>";			
			}			
		}else if ($_GET['aksi'] == 'tambah') {
			?>
			Masukan nama untuk user baru
			<form method='GET'>
				<input id='add_nama' type='text' autofocus='autofocus' name='nama'/>
				<input type='hidden' name='aksi' value='tombol_tambah'/>
				<input onclick="cek('add_nama');" type='submit' value='ok'>
			</form>
			<button name='cancel'><a style='text-decoration:none;' href="?aksi=cancel">cancel</a></button>
			<?php
		}else if ($_GET['aksi'] == 'cancel') {
			echo "<script>window.location = 'session-view.php?pesan=cancel';</script>";
		}
	}	
}
?>
<br/><br/>
<?php if (isset($_SESSION['logged'])) : if ($_SESSION['logged'] == true) : ?>
	<a href='?aksi=logout'>LOGOUT</a>
<?php  endif; else : ?>
	<a href='masuk.php'>LOGIN</a>	
<?php  endif; ?>
